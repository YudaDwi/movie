part of 'movie_bloc.dart';

@immutable
abstract class MovieState extends Equatable {}

class MovieInitial extends MovieState {
  @override
  List<Object> get props => [];
}

class MovieLoaded extends MovieState {
  final List<Movie> movies;
  MovieLoaded(this.movies);
  @override
  List<Object> get props => [movies];
}
