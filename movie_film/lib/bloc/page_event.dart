part of 'page_bloc.dart';

@immutable
abstract class PageEvent extends Equatable {}

class GoToSplashPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GoToMainPage extends PageEvent {
  @override
  List<Object> get props => [];
}
