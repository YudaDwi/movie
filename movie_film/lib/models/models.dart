import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'movie.dart';
part 'genre.dart';
part 'credits.dart';
part 'movie_details.dart';
