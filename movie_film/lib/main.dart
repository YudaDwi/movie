import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_film/bloc/bloc.dart';
import 'package:movie_film/ui/pages/pages.dart';
// import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => MovieBloc()..add(FetchMovie())),
        BlocProvider(create: (_) => PageBloc())
      ],
      child: MaterialApp(
        home: SplashPage(),
      ),
    );
  }
}
