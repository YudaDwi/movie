import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:movie_film/bloc/bloc.dart';
import 'package:movie_film/models/models.dart';
import 'package:movie_film/services/services.dart';
import 'package:movie_film/shared/shared.dart';
import 'package:movie_film/ui/widgets/widgets.dart';

part 'wrapper.dart';
part 'main_page.dart';
part 'splash_page.dart';
part 'movie_detail_page.dart';
