part of 'widgets.dart';

class MovieCard extends StatelessWidget {
  final Movie movie;
  final Function onTap;

  MovieCard(this.movie, {this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          //1c kalau onTap sudah ditekan maka ia mengembalikan fungsi onTap
          if (onTap != null) {
            onTap();
          }
        },
        child: Container(
          height: 568,
          width: 310,
          child: Column(
            children: [
              Container(
                height: 459,
                width: 310,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    image: DecorationImage(
                        image: NetworkImage(
                            imageBaseUrl + 'w780' + movie.backdropPath),
                        fit: BoxFit.cover)),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                movie.title,
                style: blackTextFont.copyWith(
                    fontSize: 15, fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: 3,
              ),
              RatingStar(
                voteAverage: movie.voteAverage,
              )
            ],
          ),
        ));
  }
}
