part of 'models.dart';

class Movie extends Equatable {
  final int id; //untuk id movie
  final String title; //untuk judul movie
  final double voteAverage; //untuk rating nya
  final String overView; //untuk deskripsi
  final String posterPath; //untuk gambar posternya
  final String backdropPath; //untuk backdrop pathnya

  Movie(
      {@required this.id,
      @required this.title,
      @required this.voteAverage,
      @required this.overView,
      @required this.posterPath,
      @required this.backdropPath});

  factory Movie.fromJson(Map<String, dynamic> json) => Movie(
      id: json['id'],
      title: json['title'],
      voteAverage: (json['vote_average'] as num).toDouble(),
      overView: json['overview'],
      posterPath: json['poster_path'],
      backdropPath: json['backdrop_path']);

  @override
  List<Object> get props =>
      [id, title, voteAverage, overView, posterPath, backdropPath];
}
