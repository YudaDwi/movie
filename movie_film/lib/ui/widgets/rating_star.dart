part of 'widgets.dart';

class RatingStar extends StatelessWidget {
  final double voteAverage; //1d vote average adl angka rating
  final double starSize; //ukurang bintang
  final double fontSize;
  final Color color;
  final MainAxisAlignment alignment;

  RatingStar(
      {this.voteAverage = 0,
      this.starSize = 20,
      this.fontSize = 18,
      this.color,
      this.alignment = MainAxisAlignment.center});
  @override
  Widget build(BuildContext context) {
    int n = (voteAverage / 2).round();
    //rating dibagi 2 lalu dibulatkan
    List<Widget> widgets = List.generate(
        5,
        (index) => Icon(
              index < n ? MdiIcons.star : MdiIcons.starOutline,
              color: Colors.yellow,
              size: starSize,
            ));
    widgets.add(SizedBox(
      width: 8,
    ));
    widgets.add(Text(
      '$voteAverage/10',
      style: whiteTextFont.copyWith(fontSize: 18),
    ));
    return Row(
      children: widgets,
    );
  }
}
