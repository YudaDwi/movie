part of 'models.dart';

class Genres extends Equatable {
  final String genres;

  Genres(this.genres);
  @override
  List<Object> get props => [genres];
}

List<Genres> dummyGenres = [
  Genres('Action'),
  Genres('Crime'),
  Genres('Comedy')
];
