import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:movie_film/models/models.dart';
import 'package:movie_film/shared/shared.dart';

part 'button_genres.dart';
part 'movie_card.dart';
part 'rating_star.dart';
part 'credit_card.dart';
