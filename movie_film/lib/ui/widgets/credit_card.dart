part of 'widgets.dart';

class CreditCard extends StatelessWidget {
  final Credit credit;
  CreditCard(this.credit);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 80,
          width: 80,
          decoration: BoxDecoration(
              color: Colors.blueGrey[100],
              shape: BoxShape.circle,
              image: (credit.profilePath == null)
                  ? null
                  : DecorationImage(
                      image: NetworkImage(
                          imageBaseUrl + 'w185' + credit.profilePath),
                      fit: BoxFit.cover)),
        ),
        Container(
          margin: EdgeInsets.only(top: 12),
          width: 80,
          child: Text(
            credit.name,
            style: blackTextFont.copyWith(fontSize: 16),
            maxLines: 2,
            textAlign: TextAlign.center,
            overflow: TextOverflow.clip,
          ),
        )
      ],
    );
  }
}
