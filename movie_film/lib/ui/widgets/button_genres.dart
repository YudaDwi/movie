part of 'widgets.dart';

class ButtonGenres extends StatelessWidget {
  final Genres genres;

  ButtonGenres(this.genres);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 105,
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(50)),
    );
  }
}
