part of 'pages.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            //icon splash
            Container(
              height: 136,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/help_centre.png'))),
            ),
            Container(
              margin: EdgeInsets.only(top: 70, bottom: 16),
              child: Text(
                'Pengalaman Baru',
                style: blackTextFont.copyWith(fontSize: 20),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 70),
              child: Text(
                'Film Baru\nDengan Pengamanan Baru',
                style: greyTextFont.copyWith(
                    fontSize: 16, fontWeight: FontWeight.w300),
                textAlign: TextAlign.center,
              ),
            ),
            Container(
              height: 46,
              width: 250,
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(8)),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.purple),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => MainPage()));
                  },
                  child: Text(
                    'Mulai',
                    style: whiteTextFont.copyWith(fontSize: 16),
                  )),
            )
          ],
        ),
      ),
    );
  }
}
