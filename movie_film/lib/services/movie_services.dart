part of 'services.dart';

class MovieServices {
  static Future<List<Movie>> getMovies(int page, {http.Client client}) async {
    var url = Uri.parse(
        'https://api.themoviedb.org/3/discover/movie?api_key=$apiKey&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=$page');

    client ??= http.Client();

    var response = await client.get(url);
    if (response.statusCode != 200) {
      return [];
    }

    var data = json.decode(response.body);

    List result = data['results'];

    return result.map((e) => Movie.fromJson(e)).toList();
  }

  static Future<List<Credit>> getCredit(int movieID,
      {http.Client client}) async {
    var url = Uri.parse(
        "https://api.themoviedb.org/3/movie/$movieID/credits?api_key=$apiKey");

    client ??= http.Client();
    var response = await client.get(url);

    var data = json.decode(response.body);

    //lumayan ribet
    return ((data as Map<String, dynamic>)['cast'] as List)
        .map((e) => Credit(
            name: (e as Map<String, dynamic>)['name'],
            profilePath: (e as Map<String, dynamic>)['profile_path']))
        .take(8)
        .toList();
  }

  //perubahan terjadi pada penulisan http
  //var url = Uri.parse pd sebelumnya hanya menggunakan string url = url tsb
  static Future<MovieDetail> getDetails(Movie movie,
      {http.Client client}) async {
    var url = Uri.parse(
        'https://api.themoviedb.org/3/movie/${movie.id}?api_key=$apiKey&language=en-US');

    client ??= http.Client();
    //6a client di cek, kalau dia null maka akan diisi httpClient

    var response = await client.get(url);
    //6a salah satu method pemanggilan api dan ini men gate url

    var data = json.decode(response.body);
    //6a bila response berhasil maka

    List genres = (data as Map<String, dynamic>)['genres'];
    //6a karena ini di movie detail berbentuk list dan didalam list
    //berbentuk map lalu key dan value
    String language;
    //6a ini merubah data karena key original_language adl fi
    switch ((data as Map<String, dynamic>)['original_language'].toString()) {
      case 'ja':
        language = 'Japanese';
        break;
      case 'id':
        language = 'Indonesia';
        break;
      case 'ko':
        language = 'Korean';
        break;
      case 'en':
        language = 'English';
        break;
    }
    return MovieDetail(movie,
        language: language,
        genres: genres
            .map((e) => (e as Map<String, dynamic>)['name'].toString())
            .toList());
    //6a jadi setiap element di dalam genres itu adl map juga
  }
}
