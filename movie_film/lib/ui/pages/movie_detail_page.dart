part of 'pages.dart';

class MovieDetailPage extends StatelessWidget {
  final Movie movie;
  MovieDetailPage(this.movie);
  @override
  Widget build(BuildContext context) {
    MovieDetail movieDetail;
    List<Credit> credits;

    return Scaffold(
      body: Stack(
        children: [
          Container(
            color: Colors.purple,
          ),
          SafeArea(
              child: Container(
            color: Colors.white,
          )),
          ListView(
            children: [
              FutureBuilder(
                  future: MovieServices.getDetails(movie),
                  builder: (_, snapshot) {
                    if (snapshot.hasData) {
                      movieDetail = snapshot.data;
                      return Column(
                        children: [
                          Stack(
                            children: [
                              //backdrop
                              Stack(
                                children: [
                                  Container(
                                    height: 352,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(20),
                                            bottomRight: Radius.circular(20)),
                                        image: DecorationImage(
                                            image: NetworkImage(imageBaseUrl +
                                                    'w1280' +
                                                    movie.backdropPath ??
                                                movie.backdropPath),
                                            fit: BoxFit.cover)),
                                  )
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    top: 56, left: defaultMargin),
                                padding: EdgeInsets.all(1),
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadiusDirectional.circular(5),
                                    color: Colors.transparent),
                                child: GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        (MaterialPageRoute(builder: (context) {
                                          return MainPage();
                                        })));
                                  },
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            ],
                          ),
                          //judul film
                          Container(
                              margin: EdgeInsets.fromLTRB(
                                defaultMargin,
                                30,
                                defaultMargin,
                                20,
                              ),
                              child: Text(
                                movie.title,
                                style: blackTextFont.copyWith(
                                    fontSize: 32, fontWeight: FontWeight.w600),
                              )),
                          //plot summary
                          Container(
                            margin: EdgeInsets.fromLTRB(
                                defaultMargin, 0, defaultMargin, 16),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                'Plot Summary',
                                style: blackTextFont.copyWith(fontSize: 24),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(
                                defaultMargin, 0, defaultMargin, 20),
                            child: Text(
                              movie.overView,
                              style: greyTextFont.copyWith(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                            ),
                          ),
                          //Cast & Crew
                          Container(
                            margin: EdgeInsets.fromLTRB(
                                defaultMargin, 0, defaultMargin, 16),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                'Cast & Crew',
                                style: blackTextFont.copyWith(fontSize: 24),
                              ),
                            ),
                          ),
                          //foto pemain film
                          FutureBuilder(
                              future: MovieServices.getCredit(movie.id),
                              builder: (_, snapshot) {
                                if (snapshot.hasData) {
                                  credits = snapshot.data;
                                  return SizedBox(
                                    height: 153,
                                    child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemCount: credits.length,
                                        itemBuilder: (_, index) {
                                          return Container(
                                            margin: EdgeInsets.only(
                                                left: (index == 0)
                                                    ? defaultMargin
                                                    : 0,
                                                right: (index ==
                                                        credits.length - 1)
                                                    ? defaultMargin
                                                    : 16),
                                            child: CreditCard(credits[index]),
                                          );
                                        }),
                                  );
                                } else {
                                  return SizedBox();
                                }
                              })
                        ],
                      );
                    } else {
                      return SizedBox();
                    }
                  })
            ],
          )
        ],
      ),
    );
  }
}
