part of 'pages.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            color: Colors.white,
          ),
          SafeArea(
              child: Container(
            color: Colors.white,
          )),
          ListView(
            children: [
              Container(
                margin:
                    EdgeInsets.only(right: defaultMargin, left: defaultMargin),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //logo searching dan titik 3
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [Icon(Icons.notes_outlined)],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 30),
                      child: Text(
                        'Menu Film\nHari Ini ?',
                        style: blackTextFont.copyWith(fontSize: 24),
                      ),
                    ),
                    SizedBox(
                      height: 568,
                      child: BlocBuilder<MovieBloc, MovieState>(
                        builder: (_, movieState) {
                          if (movieState is MovieLoaded) {
                            List<Movie> movies =
                                movieState.movies.sublist(0, 10);
                            return ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: movies.length,
                                itemBuilder: (_, index) {
                                  return Container(
                                      margin: EdgeInsets.only(
                                          left:
                                              (index == 0) ? defaultMargin : 0,
                                          right: (index == movies.length - 1)
                                              ? defaultMargin
                                              : 16),
                                      child: MovieCard(
                                        movies[index],
                                        onTap: () {
                                          Navigator.push(context,
                                              MaterialPageRoute(
                                                  builder: (context) {
                                            return MovieDetailPage(
                                                movies[index]);
                                          }));
                                        },
                                      ));
                                });
                          } else {
                            return SpinKitFadingCircle(
                              color: Colors.purple,
                              size: 50,
                            );
                          }
                        },
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}

// MovieCard(movies[index]
