part of 'models.dart';

class Credit extends Equatable {
  final String name; //nama dari aktor/aktris
  final String profilePath; //foto dari aktor/aktris

  Credit({this.name, this.profilePath});

  @override
  List<Object> get props => [name, profilePath];
}
