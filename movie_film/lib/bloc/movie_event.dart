part of 'movie_bloc.dart';

@immutable
abstract class MovieEvent extends Equatable {}

class FetchMovie extends MovieEvent {
  @override
  List<Object> get props => [];
}
