part of 'pages.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        builder: (_, pageState) =>
            (pageState is OnSplashPage) ? SplashPage() : MainPage());
  }
}
